var express = require("express");
var app = express();

var homeController = require('./controllers/home');
var adminController = require("./controllers/admin");

app.set("view engine", "pug");
app.use(express.static("public"));

app.use(
    express.urlencoded({
        extended: true,
    })
);
app.use(express.json());


app.use("/", homeController);
app.use("/admin", adminController);

app.listen(5000, function() {
    console.log("Server is listening on port 5000!");
})